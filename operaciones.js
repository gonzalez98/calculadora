//Declarar variables

let operando1
let operando2
let operando3
let signo

function inicio(){
    
    let problema = document.getElementById("problema");
    let resultado = document.getElementById("resultado");
    let reset = document.getElementById("reset");
    let divi = document.getElementById("divi");
    let multi = document.getElementById("multi");
    let add = document.getElementById("add");
    let sub = document.getElementById("sub");
    let percen = document.getElementById("percen");
    let equals = document.getElementById("equals");
    let raiz = document.getElementById("raiz");
    let exp = document.getElementById("exp");
    let point = document.getElementById("point");
    let nine = document.getElementById("nine");
    let eight = document.getElementById("eight");
    let seven = document.getElementById("seven");
    let six = document.getElementById("six");
    let five = document.getElementById("five");
    let four = document.getElementById("four");
    let three = document.getElementById("three");
    let two = document.getElementById("two");
    let one = document.getElementById("one");
    let zero = document.getElementById("zero");


 //Usar eventos a partir de los teclados con onclick 

one.onclick = function(e){
    problema.textContent = problema.textContent  + "1";
    }
two.onclick = function(e){
    problema.textContent = problema.textContent  + "2";
    }
three.onclick = function(e){
    problema.textContent = problema.textContent + "3";
    }
four.onclick = function(e){
    problema.textContent = problema.textContent  + "4";
    }
five.onclick = function(e){
    problema.textContent = problema.textContent  + "5";
    }
six.onclick = function(e){
    problema.textContent = problema.textContent  + "6";
    }
seven.onclick = function(e){
    problema.textContent = problema.textContent  + "7";
    }
eight.onclick = function(e){
    problema.textContent = problema.textContent  + "8";
    }
nine.onclick = function(e){
    problema.textContent = problema.textContent  + "9";
    }
zero.onclick = function(e){
    problema.textContent = problema.textContent  + "0";
    }


point.onclick = function(e){
    problema.textContent = problema.textContent  + ".";
    }

//operaciones

raiz.onclick = function(e){
    operando1 = problema.textContent ;
    operando3 = "r";
    op.textContent = "√"
    } 

exp.onclick = function(e){
    operando1 = problema.textContent ;
    operando3 = "^";
    op.textContent = "^"
    limpiar();
    } 


divi.onclick = function(e){
    operando1 = problema.textContent ;
    operando3 = "/";
    op.textContent = "/"
    limpiar();
    } 

multi.onclick = function(e){
    operando1 = problema.textContent ;
    operando3 = "*";
    op.textContent = "*"
    limpiar();
    } 

add.onclick = function(e){
    operando1 = problema.textContent ;
    operando3 = "+";
    op.textContent = "+"
    limpiar();
    } 

sub.onclick = function(e){
    operando1 = problema.textContent ;
    operando3 = "-";
    op.textContent = "-"
    limpiar();
    } 

percen.onclick = function(e){
    operando1 = problema.textContent ;
    operando3 = "%";
    op.textContent = "%"
    limpiar();
    } 

//igual 

equals.onclick = function(e){
    operando2= problema.textContent;
    resolver();
    limpiarS();
    }

    
//C 

reset.onclick = function(e){
    resetear();
}
}

//--------------------------------------------------
function limpiarS(){
    op.textContent = "";
}

function limpiar(){
    problema.textContent = "";
}

function resetear(){
    resultado.textContent = "";
    problema.textContent = "";
    op.textContent = "";
    operando1 = 0;
    operando2 = 0;
    operando3 = "";
}

function resolver(){
    var r = 0;
    switch(operando3){
      case "+":
        r = parseFloat(operando1) + parseFloat(operando2);
        break;
      case "-":
        r = parseFloat(operando1) - parseFloat(operando2);
        break;
      case "*":
        r = parseFloat(operando1) * parseFloat(operando2);
        break;
      case "/":
        r = parseFloat(operando1) / parseFloat(operando2);
        break;
      case "r":
        r = Math.sqrt(operando1);
        break;
      case "^":
        r = Math.pow(operando1, operando2);
        break;
      case "%":
        r = parseFloat(operando1) / 100;
        break;        
        
    }
    resultado.textContent = r;
}







